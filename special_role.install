<?php

/**
 * @file
 * Create tables for entity and entity revisions. Attach fields to the entity.
 */

/**
 * Implements hook_schema().
 */
function special_role_schema() {
  $schema = array();

  // Table for storing data of entities.
  $schema['special_role'] = array(
    'description' => 'The base table for Special roles.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'rid' => array(
        'description' => 'The {role}.rid of the corresponding Drupal role.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'type' => array(
        'description' => 'Entity bundle.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => 'Role title.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'Role description.',
        'type' => 'text',
      ),
      'extra' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Serialized data.',
        'serialize' => TRUE,
        'serialized default' => 'a:0:{}',
      ),
    ),
    'primary key' => array('id'),
  );

  // Table for storing data of entity exportable bundles.
  $schema['special_role_type'] = array(
    'description' => 'The base table for data of bundles of "special_role" entities.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier.',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'Bundle machine name.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'Human-readable name of bundle.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of bundle.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
      'extra' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Serialized data.',
        'serialize' => TRUE,
        'serialized default' => 'a:0:{}',
      ),
    // Copy and paste of entity_exportable_schema_fields().
    ) + array(
      'status' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0x01,
        'size' => 'tiny',
        'description' => 'The exportable status of the entity.',
      ),
      'module' => array(
        'description' => 'The name of the providing module if the entity has been defined in code.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );

  return $schema;
}
