<?php

/**
 * @file
 * Callbacks for administrating entities.
 */

/**
 * Choose bundle of entity to add.
 *
 * @return array
 *   Array describing a list of bundles to render.
 */
function special_role_choose_bundle() {
  // Show list of all existing entity bundles.
  $items = array();
  foreach (special_role_type_load_multiple() as $entity_type_key => $entity_type) {
    $items[] = l(entity_label('special_role_type', $entity_type), special_role_path(NULL, 'add') . '/' . $entity_type_key);
  }
  return array(
    'list' => array(
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => t('Choose type of role to add.'),
    ),
  );
}

/**
 * Form constructor for the entity add form.
 *
 * @param string $type
 *   Entity type (bundle) to add.
 *
 * @return array
 *   Entity edit form.
 */
function special_role_add($type) {
  $entity_type = special_role_type_load_multiple($type);
  drupal_set_title(t('Create @name role', array('@name' => entity_label('special_role_type', $entity_type))));

  // Return form for the given entity bundle.
  $entity = entity_create('special_role', array('type' => $type));
  $output = drupal_get_form('special_role_form', $entity);
  return $output;
}

/**
 * Form constructor for the entity edit form.
 *
 * In this form we shall manually define all form elements related to editable
 * properties of the entity.
 *
 * @param object $entity
 *   Entity to edit.
 *
 * @return array
 *   Entity edit form.
 */
function special_role_form($form, &$form_state, $entity) {
  // Store the entity in the form.
  $form_state['entity'] = $entity;

  $type = $entity->type;
  $type_entity = special_role_type_load($type);

  // Describe all properties of the entity which shall be shown on the form.
  $wrapper = entity_metadata_wrapper('special_role', $entity);
  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $wrapper->title->value(),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $wrapper->description->value(),
  );
  $extra = !empty($entity->extra) ? $entity->extra : array();
  $form['extra'] = array(
    '#type' => 'value',
    '#value' => $extra,
  );

  $rid = 0;
  if (!empty($entity->rid)) {
    $rid = $entity->rid;
  }
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $rid,
  );

  if (!empty($entity->id)) {
    $perms = isset($extra['perms']) ? $extra['perms'] : array();
    $col_names = array(
      'access' => t('Access'),
    );
    $limit_permissions = array();
    if ($type_entity && !empty($type_entity->extra)) {
      if (!empty($type_entity->extra['perms']['optional'])) {
        $limit_permissions = $type_entity->extra['perms']['optional'];
      }
      if (!empty($type_entity->extra['perm_modules']['optional'])) {
        $limit_permissions += drupal_map_assoc(array_keys(_special_role_get_permissions($type_entity->extra['perm_modules']['optional'])));
      }
    }
    // Add base permissions
    $form['perms'] = _special_role_user_admin_permissions($form_state, $col_names, $perms, $limit_permissions);
    $form['perms']['#tree'] = TRUE;
  }

  // Add fields of the entity to the form.
  field_attach_form('special_role', $entity, $form, $form_state);

  // Add some buttons.
  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }
  $form['actions'] = array(
    '#weight' => 100,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save role'),
    '#submit' => $submit + array('special_role_form_submit'),
  );
  $entity_id = entity_id('special_role', $entity);
  if (!empty($entity_id)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('special_role_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Entity edit form "Save" submit handler.
 */
function special_role_form_submit($form, &$form_state) {
  // Automatically get edited entity from the form and save it.
  // @see entity_form_submit_build_entity()
  $entity = $form_state['entity'];
  entity_form_submit_build_entity('special_role', $entity, $form, $form_state);
  // Shuffle perms into extra.
  if (!is_array($entity->extra)) {
    $entity->extra = array();
  }
  $entity->extra['perms'] = array(
    'access' => array(),
  );
  if (!empty($entity->perms['checkboxes']['access'])) {
    $entity->extra['perms']['access'] = array_filter($entity->perms['checkboxes']['access']);
  }
  unset($entity->perms);
  entity_save('special_role', $entity);

  // Redirect user to edited entity page.
  $entity_uri = entity_uri('special_role', $entity);
  $form_state['redirect'] = $entity_uri['path'];
  drupal_set_message(t('Role %title saved.', array('%title' => entity_label('special_role', $entity))));
}

/**
 * Entity edit form "Delete" submit handler.
 */
function special_role_form_submit_delete($form, &$form_state) {
  // Redirect user to "Delete" URI for this entity.
  $entity = $form_state['entity'];
  $entity_uri = entity_uri('special_role', $entity);
  $form_state['redirect'] = $entity_uri['path'] . '/delete';
}

/**
 * Form constructor for the entity delete confirmation form.
 *
 * @param object $entity
 *   Entity to delete.
 *
 * @return array
 *   Confirmation form.
 */
function special_role_delete_form($form, &$form_state, $entity) {
  // Store the entity in the form.
  $form_state['entity'] = $entity;

  // Show confirm dialog.
  $entity_uri = entity_uri('special_role', $entity);
  $message = t('Are you sure you want to delete role %title?', array('%title' => entity_label('special_role', $entity)));
  return confirm_form(
    $form,
    $message,
    $entity_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Entity "Confirm delete" form submit handler.
 */
function special_role_delete_form_submit($form, &$form_state) {
  // Delete the entity.
  $entity = $form_state['entity'];
  entity_delete('special_role', entity_id('special_role', $entity));

  // Redirect user.
  drupal_set_message(t('Role %title deleted.', array('%title' => entity_label('special_role', $entity))));
  $form_state['redirect'] = 'admin/people/special-role-' . $entity->type;
}

/**
 * Form constructor for the entity type edit form.
 *
 * In this form we shall manually define all form elements related to editable
 * properties of the entity.
 */
function special_role_type_form($form, &$form_state, $entity_type, $op = 'edit') {
  // Handle the case when cloning is performed.
  if ($op == 'clone') {
    $entity_type->label .= ' (cloned)';
    $entity_type->type = '';
  }

  // Describe all properties of the entity which shall be shown on the form.
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $entity_type->label,
    '#description' => t('The human-readable name of this special role type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($entity_type->type) ? $entity_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $entity_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'special_role_type_load_multiple',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this special role type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($entity_type->description) ? $entity_type->description : '',
    '#description' => t('Description about the special role type.'),
  );
  $extra = !empty($entity_type->extra) ? $entity_type->extra : array();
  $form['extra'] = array(
    '#type' => 'value',
    '#value' => $extra,
  );

  if (!empty($entity_type->id)) {
    $perms = isset($extra['perms']) ? $extra['perms'] : array();
    $col_names = array(
      'base' => t('Base'),
      'optional' => t('Optional'),
    );
    // Add base permissions
    $form['perms'] = _special_role_user_admin_permissions($form_state, $col_names, $perms);
    $form['perms']['#tree'] = TRUE;

    // And include those from a whole module.
    $modules_options = drupal_map_assoc(module_implements('permission'));
    $form['perm_modules'] = array(
      '#type' => 'fieldset',
      '#title' => t('Modules'),
      '#description' => t('In <strong>addition</strong> to the permissions selected above, it is possible to choose to support all permissions declared by a module here.'),
      '#tree' => TRUE,
      'base' => array(
        '#type' => 'select',
        '#title' => t('Base'),
        '#multiple' => TRUE,
        '#options' => $modules_options,
        '#default_value' => isset($extra['perm_modules']['base']) ? $extra['perm_modules']['base'] : array(),
        '#description' => t('<em>Base</em> permissions are always granted to derived roles and are not configurable.'),
      ),
      'optional' => array(
        '#type' => 'select',
        '#title' => t('Optional'),
        '#multiple' => TRUE,
        '#options' => $modules_options,
        '#default_value' => isset($extra['perm_modules']['optional']) ? $extra['perm_modules']['optional'] : array(),
        '#description' => t('<em>Optional</em> permissions can be configured on the roles themselves.'),
      ),
    );
  }

  // Advanced options.
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#tree' => TRUE,
  );
  $form['advanced']['exclude_from_features'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exclude optional permissions from features exports'),
    '#description' => t('When ticked features exports of any optional permissions will exclude roles of this type.'),
    '#default_value' => !empty($extra['advanced']['exclude_from_features']),
  );
  if (module_exists('roleassign')) {
    $form['advanced']['roleassign'] = array(
      '#type' => 'checkbox',
      '#title' => t('Assignable roles'),
      '#description' => t('Make all roles of this type "assignable" with the roleassign module.'),
      '#default_value' => !empty($extra['advanced']['roleassign']),
    );
  }

  // Add some buttons.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Special role type'),
    '#weight' => 40,
  );
  if (!$entity_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Special role type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('special_role_type_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Helper to embed a user permissions form.
 */
function _special_role_user_admin_permissions(&$form_state, $col_names, $col_values, $permissions_limit = NULL) {

  if (isset($permissions_limit)) {
    $permissions_limit = drupal_map_assoc($permissions_limit);
  }

  form_load_include($form_state, 'admin.inc', 'user');
  $options = array();
  $form = array(
    '#type' => 'fieldset',
    '#title' => t('Permissions'),
    '#collapsed' => FALSE,
    '#collapsable' => TRUE,
    '#theme' => 'special_role_user_admin_permissions',
  );
  $form['cols'] = array(
    '#type' => 'value',
    '#value' => $col_names,
  );
  $module_info = system_get_info('module');
  $hide_descriptions = system_admin_compact_mode();
  $modules = array();
  foreach (module_implements('permission') as $module) {
    $modules[$module] = $module_info[$module]['name'];
  }
  asort($modules);

  foreach ($modules as $module => $display_name) {
    if ($permissions = module_invoke($module, 'permission')) {
      if (isset($permissions_limit)) {
        $permissions = array_intersect_key($permissions, $permissions_limit);
      }
      if (!empty($permissions)) {
        $form['permission'][] = array(
          '#markup' => $module_info[$module]['name'],
          '#id' => $module,
        );
        foreach ($permissions as $perm => $perm_item) {
          // Fill in default values for the permission.
          $perm_item += array(
            'description' => '',
            'restrict access' => FALSE,
            'warning' => !empty($perm_item['restrict access']) ? t('Warning: Give to trusted roles only; this permission has security implications.') : '',
          );
          $options[$perm] = '';
          $form['permission'][$perm] = array(
            '#type' => 'item',
            '#markup' => $perm_item['title'],
            '#description' => theme('user_permission_description', array('permission_item' => $perm_item, 'hide' => $hide_descriptions)),
          );
          foreach ($col_names as $rid => $name) {
            // Builds arrays for checked boxes for each role
            if (isset($col_values[$rid][$perm])) {
              $status[$rid][] = $perm;
            }
          }
        }
      }
    }
  }
  foreach ($col_names as $rid => $name) {
    $form['checkboxes'][$rid] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => isset($status[$rid]) ? $status[$rid] : array(),
      '#attributes' => array('class' => array('rid-' . $rid)),
    );
    $form['col_names'][$rid] = array('#markup' => check_plain($name), '#tree' => TRUE);
  }

  $form['#attached']['js'][] = drupal_get_path('module', 'user') . '/user.permissions.js';

  return $form;
}

/**
 * Returns HTML for the administer permissions page.
 */
function theme_special_role_user_admin_permissions($variables) {
  $form = $variables['form'];

  $roles = $form['cols']['#value'];
  foreach (element_children($form['permission']) as $key) {
    $row = array();
    // Module name
    if (is_numeric($key)) {
      $row[] = array('data' => drupal_render($form['permission'][$key]), 'class' => array('module'), 'id' => 'module-' . $form['permission'][$key]['#id'], 'colspan' => count($form['cols']['#value']) + 1);
    }
    else {
      // Permission row.
      $row[] = array(
        'data' => drupal_render($form['permission'][$key]),
        'class' => array('permission'),
      );
      foreach (element_children($form['checkboxes']) as $rid) {
        $form['checkboxes'][$rid][$key]['#title'] = $roles[$rid] . ': ' . $form['permission'][$key]['#markup'];
        $form['checkboxes'][$rid][$key]['#title_display'] = 'invisible';
        $row[] = array('data' => drupal_render($form['checkboxes'][$rid][$key]), 'class' => array('checkbox'));
      }
    }
    $rows[] = $row;
  }
  $header[] = (t('Permission'));
  foreach (element_children($form['col_names']) as $rid) {
    $header[] = array('data' => drupal_render($form['col_names'][$rid]), 'class' => array('checkbox'));
  }
  $output = theme('system_compact_link');
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'permissions')));
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Entity type edit form "Save" submit handler.
 */
function special_role_type_form_submit(&$form, &$form_state) {
  // Automatically get edited entity type from the form and save it.
  // @see entity_ui_form_submit_build_entity()
  $entity_type = entity_ui_form_submit_build_entity($form, $form_state);
  if (!is_array($entity_type->extra)) {
    $entity_type->extra = array();
  }
  // Shuffle perms into extra.
  $entity_type->extra['perms'] = array(
    'base' => array(),
    'optional' => array(),
  );
  if (!empty($entity_type->perms['checkboxes']['base'])) {
    $entity_type->extra['perms']['base'] = array_filter($entity_type->perms['checkboxes']['base']);
  }
  if (!empty($entity_type->perms['checkboxes']['optional'])) {
    $entity_type->extra['perms']['optional'] = array_filter($entity_type->perms['checkboxes']['optional']);
  }
  unset($entity_type->perms);
  // Perms modules.
  $entity_type->extra['perm_modules'] = array(
    'base' => !empty($entity_type->perm_modules['base']) ? $entity_type->perm_modules['base'] : array(),
    'optional' => !empty($entity_type->perm_modules['optional']) ? $entity_type->perm_modules['optional'] : array(),
  );
  unset($entity_type->perm_modules);
  // Advanced.
  $entity_type->extra['advanced'] = array();
  if (!empty($entity_type->advanced)) {
    $entity_type->extra['advanced'] = $entity_type->advanced;
  }
  unset($entity_type->advanced);
  entity_save('special_role_type', $entity_type);

  // Redirect user.
  $form_state['redirect'] = 'admin/structure/special_role';
}

/**
 * Entity edit form "Delete" submit handler.
 */
function special_role_type_form_submit_delete(&$form, &$form_state) {
  // Redirect user to "Delete" URI for this entity type.
  $form_state['redirect'] = 'admin/structure/special_role/' . $form_state['special_role_type']->type . '/delete';
}

/**
 * Form constructor for the entity type delete confirmation form.
 *
 * @param object $entity_type
 *   Entity type to delete.
 *
 * @return array
 *   Confirmation form.
 */
function special_role_type_form_delete_confirm($form, &$form_state, $entity_type) {
  // Store the entity in the form.
  $form_state['entity_type'] = $entity_type;

  // Show confirm dialog.
  $message = t('Are you sure you want to delete special role type %title?', array('%title' => entity_label('special_role_type', $entity_type)));
  return confirm_form(
    $form,
    $message,
    'special_role/' . entity_id('special_role_type', $entity_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Entity type "Confirm delete" form submit handler.
 */
function special_role_type_form_delete_confirm_submit($form, &$form_state) {
  // Delete the entity type.
  $entity_type = $form_state['entity_type'];
  entity_delete('special_role_type', entity_id('special_role_type', $entity_type));

  // Redirect user.
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $entity_type->type, '%title' => $entity_type->label)));
  $form_state['redirect'] = 'admin/structure/special_role';
}
