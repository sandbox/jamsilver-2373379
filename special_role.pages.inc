<?php

/**
 * @file
 * Callbacks for viewing entities.
 */

/**
 * Entity view callback.
 * 
 * @param object $entity
 *   Entity to view.
 * 
 * @return array
 *   Renderable array.
 */
function special_role_view($entity) {
  drupal_set_title(entity_label('special_role', $entity));

  // Return automatically generated view page.
  return entity_view('special_role', array(entity_id('special_role', $entity) => $entity), 'full');
}
